﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSP_IntVersion
{
    public class Population
    {
        private int genomLength;

        /// <summary>
        /// Gets List of all Individuals in Population
        /// </summary>
        public List<Individual> Individuals { get; private set; }

        /// <summary>
        /// List of all Fronts in Population
        /// </summary>
        public List<List<Individual>> Fronts { get; private set; }

        /// <summary>
        /// Total number of Individuals in Population
        /// </summary>
        public int PopulationSize { get; set; }

        // Was used for recording performance
        private double distanceTotalFitness;
        private double distanceAverageFitness;
        private double distanceStandardDeviation;

        private double costTotalFitness;
        private double costAverageFitness;
        private double costStandardDeviation;

        /// <summary>
        /// Default Constructor.
        /// For testing only, should never be used
        /// </summary>
        public Population()
        {
            Individuals = new List<Individual>();
            Fronts = new List<List<Individual>>();
            PopulationSize = 0;
            genomLength = 0;
            distanceTotalFitness = 0;
            distanceAverageFitness = 0;
            distanceStandardDeviation = 0;
            costTotalFitness = 0;
            costAverageFitness = 0;
            costStandardDeviation = 0;
        }

        /// <summary>
        /// Initializes Population of specified size of random Individuals with specified gene length
        /// </summary>
        /// <param name="size">Specified size of Population</param>
        /// <param name="n">Specified Gene Length of Individuals</param>
        public Population(int size, int n)
        {
            PopulationSize = size;
            genomLength = n;
            Individuals = new List<Individual>();
            Fronts = new List<List<Individual>>();
            distanceTotalFitness = 0;
            distanceAverageFitness = 0;
            distanceStandardDeviation = 0;
            costTotalFitness = 0;
            costAverageFitness = 0;
            costStandardDeviation = 0;
            for (int i = 0; i < PopulationSize; i++)
                Individuals.Add(new Individual(genomLength));
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="p">Population from which parameters are coppied</param>
        public Population(Population p)
        {
            PopulationSize = p.PopulationSize;
            genomLength = p.genomLength;
            distanceTotalFitness = 0;
            distanceAverageFitness = 0;
            distanceStandardDeviation = 0;
            costTotalFitness = 0;
            costAverageFitness = 0;
            costStandardDeviation = 0;
            Individuals = new List<Individual>();
            Fronts = new List<List<Individual>>();
        }

        /// <summary>
        /// Returns Individual in population with Max Distance
        /// </summary>
        /// <returns></returns>
        public Individual GetDistanceMaxFit()
        {
            Individual maxfit = this.Fronts[0].FirstOrDefault();
            foreach (var front in Fronts)
                foreach (var individual in front)
                    if (maxfit.FitnessDistance < individual.FitnessDistance)
                        maxfit = individual;
            return maxfit;
        }

        /// <summary>
        /// Returns Individual in population with Min Distance
        /// </summary>
        /// <returns></returns>
        public Individual GetDistanceMinFit()
        {
            Individual minfit = this.Fronts[0].FirstOrDefault();
            foreach (var front in Fronts)
                foreach (var individual in front)
                    if (minfit.FitnessDistance > individual.FitnessDistance)
                        minfit = individual;
            return minfit;
        }

        /// <summary>
        /// Returns Individual in population with Max Cost
        /// </summary>
        /// <returns></returns>
        public Individual GetCostMaxFit()
        {
            Individual maxfit = this.Fronts[0].FirstOrDefault();
            foreach (var front in Fronts)
                foreach (var individual in front)
                    if (maxfit.FitnessCost < individual.FitnessCost)
                        maxfit = individual;
            return maxfit;
        }

        /// <summary>
        /// Returns Individual in population with Min Cost
        /// </summary>
        /// <returns></returns>
        public Individual GetCostMinFit()
        {
            Individual minfit = this.Fronts[0].FirstOrDefault();
            foreach (var front in Fronts)
                foreach (var individual in front)
                    if (minfit.FitnessCost > individual.FitnessCost)
                        minfit = individual;
            return minfit;
        }

        /// <summary>
        /// Updates both Distance and Cost Fitness of all Individuals in Population
        /// </summary>
        public void UpdateFitnessForAll()
        {
            foreach (Individual i in Individuals)
                i.CalculateFitness();
        }

        /// <summary>
        /// Was used for recording performance
        /// </summary>
        public void UpdatePopulationData()
        {
            distanceTotalFitness = 0;
            costTotalFitness = 0;
            foreach (Individual i in Individuals)
            {
                distanceTotalFitness += i.FitnessDistance;
                costTotalFitness += i.FitnessCost;
            }
            distanceAverageFitness = distanceTotalFitness / PopulationSize;
            costAverageFitness = costTotalFitness / PopulationSize;
            distanceStandardDeviation = 0;
            costStandardDeviation = 0;
            foreach (Individual i in Individuals)
            {
                distanceStandardDeviation += Math.Pow(i.FitnessDistance - distanceAverageFitness, 2);
                costStandardDeviation += Math.Pow(i.FitnessCost - costAverageFitness, 2);
            }
            distanceStandardDeviation /= PopulationSize;
            costStandardDeviation /= PopulationSize;
            distanceStandardDeviation = Math.Sqrt(distanceStandardDeviation);
            costStandardDeviation = Math.Sqrt(costStandardDeviation);
        }

        /// <summary>
        /// Determines to which front each Individual in Population belongs to 
        /// </summary>
        public void CalculateRank() 
        {
            List<Individual> currentFront = new List<Individual>();
            foreach (var p in Individuals)
            {
                p.DominatedByCount = 0;
                p.DominatesList.Clear();
                foreach (var q in Individuals)
                {
                    if (p.Dominates(q))
                        p.DominatesList.Add(q);
                    else if (q.Dominates(p))
                        p.DominatedByCount++;
                }
                if (p.DominatedByCount == 0)
                {
                    p.Rank = 1;
                    currentFront.Add(p);
                }
            }
            int i = 1;

            while (currentFront.Count != 0)
            {
                Fronts.Add(currentFront);
                List<Individual> newFront = new List<Individual>();
                foreach (var p in currentFront)
                {
                    foreach (var q in p.DominatesList)
                    {
                        q.DominatedByCount--;
                        if (q.DominatedByCount == 0)
                        {
                            q.Rank = i + 1;
                            newFront.Add(q);
                        }
                    }
                }
                i++;
                currentFront = newFront;
            }
        }

        /// <summary>
        /// Calculates Crowding Distance of all Individuals in Population
        /// </summary>
        public void CalculateCrowdingDistance()
        {
            foreach (var front in Fronts)
            {
                foreach (var node in front)
                {
                    node.CrowdingDistance = 0;
                }
                double fitnessMaxCost = GetCostMaxFit().FitnessCost;
                double fitnessMinCost = GetCostMinFit().FitnessCost;
                double fitnessMaxDistance = GetDistanceMaxFit().FitnessDistance;
                double fitnessMinDistance = GetDistanceMinFit().FitnessDistance;

                //po cost
                List<Individual> temp = front.OrderBy(value => value.FitnessCost).ToList();
                temp[0].CrowdingDistance = Int32.MaxValue;
                temp[temp.Count - 1].CrowdingDistance = Int32.MaxValue;

                for (int i = 2; i < temp.Count - 1; i++)
                {
                    temp[i].CrowdingDistance = temp[i].CrowdingDistance +
                                                (temp[i - 1].FitnessCost + temp[i + 1].FitnessCost) /
                                                (fitnessMaxCost - fitnessMinCost);
                }

                //po distance
                temp = front.OrderBy(value => value.FitnessDistance).ToList();
                temp[0].CrowdingDistance = Int32.MaxValue;
                temp[temp.Count - 1].CrowdingDistance = Int32.MaxValue;

                for (int i = 2; i < temp.Count - 1; i++)
                {
                    temp[i].CrowdingDistance = temp[i].CrowdingDistance +
                                                (temp[i - 1].FitnessDistance + temp[i + 1].FitnessDistance) /
                                                (fitnessMaxDistance - fitnessMinDistance);
                }
                temp = front.OrderBy(value => value.CrowdingDistance).ToList();
            }
        }

        /// <summary>
        /// Selects one Individual in Population for reproducing
        /// </summary>
        /// <returns></returns>
        public Individual TournamentSelectionCrowdingDistance()
        {
            var randomAdults = new List<Individual>();
            for (var i = 0; i < TSP.tournamentSize; i++)
            {
                randomAdults.Add(Individuals[TSP.randomize.Next(PopulationSize)]);
            }
            int minRank = randomAdults[0].Rank;
            for (int i = 1; i < randomAdults.Count; i++)
            {
                if (minRank > randomAdults[i].Rank)
                    minRank = randomAdults[i].Rank;
            }

            var bestRankedAdults = new List<Individual>();
            foreach (var i in randomAdults)
                if (i.Rank == minRank)
                    bestRankedAdults.Add(i);
            bestRankedAdults = bestRankedAdults.OrderByDescending(value => value.CrowdingDistance).ToList();
            return randomAdults[0]; 
        }
    }
}
