﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Windows.Forms.DataVisualization.Charting;

namespace TSP_IntVersion
{
    public partial class TSP : Form
    {
        // Must be public static fields because I need it for ref and out
        // They should not be set outside of this class
        public static int[][] distanceMatrix;
        public static int[][] costMatrix;
        public static double tournamentSize;

        // Must be fields because I need out. 
        // Changed them from public static and used it here (instead in individual) for minor performance improvement
        private int numberofCities;
        private int numberofGenerations;
        private int populationSize;
        private double mutationRate;
        private double crossoverRate;

        public static Random randomize { get; private set; }
        
        private Population population;
        private string distanceFileName;
        private string costFileName;
        private List<string> uniqueRoutes;

        /// <summary>
        /// Default Constructor. Initializes components, random and clears charts only
        /// </summary>
        public TSP()
        {
            InitializeComponent();
            randomize = new Random();
            uniqueRoutes = new List<string>();
            currentRunChart.Series.Clear();
            paretoFrontsChart.Series.Clear();
        }

        /// <summary>
        /// Starts Genetic Alghoritm and evolutionary loop through generations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void startButton_Click(object sender, EventArgs e)
        {
            int i = 0;
            try
            {
                initializeAllValues();
            }

            catch (Exception exc)
            {
                var result = MessageBox.Show(exc.Message + " Do you wish to run with default values", "Error while initializng ", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                    initializeWithDefaultValues();
                else
                    return;
            }

            population = new Population(populationSize, numberofCities);
            population.UpdateFitnessForAll();

            // Do not forget to call these two methods inside the loop as well
            population.CalculateRank();
            population.CalculateCrowdingDistance();

            while (i++ < numberofGenerations)
            {
                Population children = new Population(population);
                while (children.Individuals.Count != population.Individuals.Count)
                {
                    Individual a = new Individual(population.TournamentSelectionCrowdingDistance());
                    Individual b = new Individual(population.TournamentSelectionCrowdingDistance());
                    if (randomize.NextDouble() < crossoverRate)
                        a.Crossover(b);
                    if (randomize.NextDouble() < mutationRate)
                        a.Mutation();
                    children.Individuals.Add(a);
                }
                children.UpdateFitnessForAll();

                // Merge the populations and get population of 2*populationSize
                foreach (var indiv in population.Individuals)
                    children.Individuals.Add(indiv);
                children.PopulationSize += population.PopulationSize;
                children.CalculateRank();
                children.CalculateCrowdingDistance();

                // Create the new population and fill it with 1*populationSize of best from merged population
                Population newPopulation = new Population(population);
                int j = 0;
                foreach (var front in children.Fronts)
                    if (newPopulation.Individuals.Count + front.Count <= newPopulation.PopulationSize)
                        foreach (var ind in front)
                            newPopulation.Individuals.Add(ind);
                    else
                    {
                        List<Individual> temp = front.OrderBy(value => value.CrowdingDistance).ToList();
                        while (newPopulation.Individuals.Count < newPopulation.PopulationSize)
                            newPopulation.Individuals.Add(temp[j++]);
                        break;
                    }

                population = newPopulation;
                population.CalculateRank();
                population.CalculateCrowdingDistance();
            }

            chartUpdate(currentRunChart, minMaxGridView);
            gridViewUpdate();
            // Reset to enable running multiple times with different sizes of Excel matrix files
            numberofCities = 0;
        }

        /// <summary>
        /// Gets the values from all UI inputs
        /// </summary>
        /// <throws>Exception if any value fails to parse or if it is not in correct format</throws>
        private void initializeAllValues()
        {

            if (!Int32.TryParse(generationsTextBox.Text, out numberofGenerations))
                throw new Exception("Error while parsing number of generations.");

            if (!Int32.TryParse(populationTextBox.Text, out populationSize))
                throw new Exception("Error while parsing population size.");

            if (!Double.TryParse(mutationTextBox.Text, out mutationRate))
                throw new Exception("Error while parsing mutation rate.");
            if (mutationRate < 0 || mutationRate > 1)
                throw new Exception("Mutation rate must be between 0 and 1.");

            if (!Double.TryParse(crossoverTextBox.Text, out crossoverRate))
                throw new Exception("Error while parsing crossover rate.");
            if (crossoverRate < 0 || crossoverRate > 1)
                throw new Exception("Crossover rate must be between 0 and 1.");

            if (!Double.TryParse(tournamentTextBox.Text, out tournamentSize))
                throw new Exception("Error while parsing tournament size.");
            if (tournamentSize >= populationSize)
                throw new Exception("Tournament size must be less than population size.");

            WriteOutExcelFile(distancePathTextBox.Text, "Sheet1", ref distanceMatrix);
            WriteOutExcelFile(costPathTextBox.Text, "Sheet1", ref costMatrix);
        }

        /// <summary>
        /// Starts algorithm with default values and default excel files 
        /// </summary>
        private void initializeWithDefaultValues()
        {
            numberofGenerations = 1000;
            populationSize = 50;
            mutationRate = 0.5;
            crossoverRate = 1;
            tournamentSize = 2;
            WriteOutExcelFile("DefaultDistance.xlsx", "Sheet1", ref distanceMatrix);
            WriteOutExcelFile("DefaultCost.xlsx", "Sheet1", ref costMatrix);
        }

        /// <summary>
        /// Imports value from Excel file into 2D array of integers
        /// </summary>
        /// <param name="fileName">Path of the Excel file</param>
        /// <param name="sheetName">Sheet of the Excel file where values are located</param>
        /// <param name="dataTable">2D array of integers where values should be imported</param>
        /// <throws>Exception if Excel files are not of same sizes or are not correctly formatted</throws>
        private void WriteOutExcelFile(string fileName, string sheetName, ref int[][] dataTable)
        {
            // Excel file can be chosen with fileOpenDialog and based on first row it will set the geneLength
            // Excel must be in form of "Donjetrougaona kvadratna matrica" // TODO translate this
            // Default Excel files must be in bin->debug folder named as DefaultDistance.xlsx and DefaultCost.xlsx

            using (var document = SpreadsheetDocument.Open(fileName, isEditable: false))
            {
                var workbookPart = document.WorkbookPart;
                var sheet = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault(s => s.Name == sheetName);
                var worksheetPart = (WorksheetPart)(workbookPart.GetPartById(sheet.Id));
                var sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();

                int i = 0;

                int dataTableDimension = sheetData.Elements<Row>().FirstOrDefault().Elements<Cell>().Count();

                // Check if both Excel files are of same sizes
                if (numberofCities == 0 || numberofCities == dataTableDimension - 1)
                    numberofCities = dataTableDimension - 1;
                else
                    throw new Exception("Distance and cost matrix are not the same dimensions.");

                //numeracije gradova u Individuals ce ici od 1 do number of cities i direktno pristupamo matricama posto se nulte ignorisu

                dataTable = new int[numberofCities + 1][]; //plus 1 jer ignorisemo prvu vrstu
                for (int r = 0; r <= numberofCities; r++)   // <= jer ignorisemo prvu kolonu
                {
                    dataTable[r] = new int[numberofCities + 1];  //plus 1 jer ignorisemo prvu kolonu
                }

                foreach (var row in sheetData.Elements<Row>())
                {
                    if (i != 0 && i != row.Elements<Cell>().Count())
                        throw new Exception(fileName + " matrix not in correct format!");
                    int j = 0;
                    foreach (var cell in row.Elements<Cell>())
                    {
                        dataTable[i][j] = GetCellValue(cell);
                        if (i > j)
                        {
                            dataTable[j][i] = dataTable[i][j];
                        }
                        j++;
                    }
                    i++;
                    if (i + 1 > dataTable.Length)
                    {
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the value as integer of cell from Excel file
        /// </summary>
        /// <param name="cell">Cell from Excel file from which value should be parsed</param>
        /// <returns>Value of cell as integer</returns>
        /// <throws>Exception if parsing fails</throws>
        private int GetCellValue(Cell cell)
        {
            var value = cell.CellFormula != null
                ? cell.CellValue.InnerText
                : cell.InnerText.Trim();

            try
            {
                return Int32.Parse(value);
            }
            catch
            {
                throw new Exception("Input matrix cell is not integer");
            }
        }
        
        /// <summary>
        /// Fills the Grid with all solutions that are found in current population
        /// </summary>
        private void gridViewUpdate()
        {
            gridViewSolutions.Rows.Clear();
            uniqueRoutes.Clear();
            int j = 1;
            foreach (var front in population.Fronts)
            {
                foreach (var individual in front)
                    if (!uniqueRoutes.Contains(individual.ToString()))
                    {
                        uniqueRoutes.Add(individual.ToString());
                        uniqueRoutes.Add(individual.ReverseToString());
                        gridViewSolutions.Rows.Add(j, individual.ToString(), individual.FitnessDistance, individual.FitnessCost, individual.Rank);
                        j++;
                    }
            }
        }

        /// <summary>
        /// Fills the Grid with Min/Max Distance/Cost Individual and marks it on chart with special character
        /// </summary>
        /// <param name="individual">Individual to be added to grid</param>
        /// <param name="chart">Chart on which individual should be marked</param>
        /// <param name="dataGridView">Grid to be filled with Individual</param>
        /// <param name="name">Name of the row in the Grid for added Individual</param>
        private void chartMinMaxUpdate(Individual individual, Chart chart, DataGridView dataGridView, string name)
        {
            chart.Series.Add(name);
            chart.Series[name].ChartType = SeriesChartType.Point;
            chart.Series[name].MarkerSize = 13;
            chart.Series[name].Points.AddXY(individual.FitnessDistance, individual.FitnessCost);
            dataGridView.Rows.Add(name, individual.FitnessDistance, individual.FitnessCost);
        }

        /// <summary>
        /// Fills the Chart and the Grid with Individuals
        /// </summary>
        /// <param name="chart">Chart to be filled with Individuals</param>
        /// <param name="dataGridView">Grid to be filled with Individuals</param>
        private void chartUpdate(Chart chart, DataGridView dataGridView)
        {
            Individual individual;
            chart.Series.Clear();
            for (int k = 0; k < population.Fronts.Count; k++)
            {
                chart.Series.Add("Rank " + (k + 1) + ": " + population.Fronts[k].Count);
                chart.Series["Rank " + (k + 1) + ": " + population.Fronts[k].Count].ChartType = SeriesChartType.Point;

                foreach (var node in population.Fronts[k])
                {
                    chart.Series["Rank " + (k + 1) + ": " + population.Fronts[k].Count].Points.AddXY(node.FitnessDistance, node.FitnessCost);
                }
            }

            dataGridView.Rows.Clear();
            individual = population.GetCostMaxFit();
            chartMinMaxUpdate(individual, chart, dataGridView, "Max Cost");
            individual = population.GetCostMinFit();
            chartMinMaxUpdate(individual, chart, dataGridView, "Min Cost");
            individual = population.GetDistanceMaxFit();
            chartMinMaxUpdate(individual, chart, dataGridView, "Max Distance");
            individual = population.GetDistanceMinFit();
            chartMinMaxUpdate(individual, chart, dataGridView, "Min Distance");
        }

        /// <summary>
        /// Copies pareto front from chart of the current run (on the right) to the global chart (on the left)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddToChart_Click(object sender, EventArgs e)
        {
            //chart3
            paretoFrontsChart.Series.Add("Run " + (paretoFrontsChart.Series.Count + 1));
            paretoFrontsChart.Series["Run " + (paretoFrontsChart.Series.Count)].ChartType = SeriesChartType.Point;
            foreach (var node in population.Fronts[0])
            {
                paretoFrontsChart.Series["Run " + (paretoFrontsChart.Series.Count)].Points.AddXY(node.FitnessDistance, node.FitnessCost);
            }
        }

        /// <summary>
        /// Clears the global chart (on the left)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClearChart_Click(object sender, EventArgs e)
        {
            paretoFrontsChart.Series.Clear();
        }

        /// <summary>
        /// Starts Open File Dialog for Distance Excel file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void browseDistanceButton_Click(object sender, EventArgs e)
        {
            if (distanceFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                distanceFileName = distanceFileDialog.FileName;
                distancePathTextBox.Text = distanceFileName;
            }
        }

        /// <summary>
        /// Starts Open File Dialog for Cost Excel file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void browseCostButton_Click(object sender, EventArgs e)
        {
            if (costFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                costFileName = costFileDialog.FileName;
                costPathTextBox.Text = costFileName;
            }
        }

        /// <summary>
        /// Toggles Grid for viewing all solutions in current population on and off
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void showHideSolutions_Click(object sender, EventArgs e)
        {
            gridViewSolutions.Visible = !gridViewSolutions.Visible;
        }
    }
}
