﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSP_IntVersion
{
    public class Individual
    {
        // Genotype Part
        private List<int> genes;
        private int geneLength;

        // Phenotype Part

        /// <summary>
        /// Gets Distance Fitness
        /// </summary>
        public double FitnessDistance { get; private set; }

        /// <summary>
        /// Gets Cost Fitness
        /// </summary>
        public double FitnessCost { get; private set; }

        /// <summary>
        /// Gets number of the front to which Individual belongs to in population
        /// </summary>
        public int Rank { get; set; }

        /// <summary>
        /// Gets Crowding Distance of the Individual in his front
        /// </summary>
        public double CrowdingDistance { get; set; }

        /// <summary>
        /// Gets the List of Individuals that are dominated by this Individual
        /// </summary>
        public List<Individual> DominatesList { get; private set; }

        /// <summary>
        /// Gets the number of Individuals that dominate this Individual
        /// </summary>
        public int DominatedByCount { get; set; }

        /// <summary>
        /// Default Constructor.
        /// For testing only, should never be used
        /// </summary>
        public Individual() 
        {
            FitnessDistance =0;
            FitnessCost=0;
            Rank=0;
            CrowdingDistance=0;
            DominatesList=new List<Individual>();
            DominatedByCount=0;
            genes = new List<int>();
            
        }

        /// <summary>
        /// Initializes random Individual with specified gene length
        /// </summary>
        /// <param name="gl">Specified gene length</param>
        public Individual(int gl)
        {
            //random gl razlicitih brojeva pocev od 1 zbog matrica 
            FitnessDistance = 0;
            FitnessCost = 0;
            Rank = 0;
            CrowdingDistance = 0;
            DominatesList = new List<Individual>();
            DominatedByCount = 0;

            genes = new List<int>();
            geneLength = gl;
            List<int> lookup = new List<int>();
            int i;
            for (i = 0; i < geneLength; i++)
                lookup.Add(i + 1);
            for (i = 0; i < geneLength; i++)
            {
                int temp = TSP.randomize.Next(lookup.Count);
                genes.Add(lookup[temp]);
                lookup.RemoveAt(temp);
            }
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="a">Individual from which parameters are coppied</param>
        public Individual(Individual a)
        {
            DominatesList = new List<Individual>();
            genes = new List<int>();
            geneLength = a.geneLength;
            for(int i=0; i<geneLength;i++)
            {
                genes.Add(a.genes[i]);
            }
        }

        /// <summary>
        /// Mutates Individual by switching places of two genes
        /// Mutation Rate was used here for whole Individual (not gene by gene) but was moved to TSP for performance
        /// </summary>
        public void Mutation()
        {
            //sansa za mutaciju je nad celom jedinkom i menjaju se samo dva random mesta, nije mesto po mesto
            int firstIndex = TSP.randomize.Next(geneLength);
            int secondIndex = TSP.randomize.Next(geneLength);
            int temp = genes[firstIndex];
            genes[firstIndex] = genes[secondIndex];
            genes[secondIndex] = temp;
        }

        /// <summary>
        /// Combines genes with another Individual with one cross point and changes this one
        /// Was returning two new Individuals before but it was changed for performance
        /// Crossover Rate was used here for but was moved to TSP for performance
        /// </summary>
        /// <param name="a">Individual with who this one combines genes</param>
        public void Crossover(Individual a)
        {
            List<int> temp = new List<int>();
            int crossPoint = TSP.randomize.Next(geneLength);
            int i;
            for (i = 0; i < crossPoint; i++)
            {
                temp.Add(this.genes[i]);
            }
            for (int j = 0; j < geneLength;j++ )
            {
                if (!temp.Contains(a.genes[j]))
                    genes[i++] = a.genes[j];
            }
        }

        /// <summary>
        /// Calculates and sets both Distance and Cost Fitness
        /// </summary>
        public void CalculateFitness()
        {
            FitnessDistance = 0;
            FitnessCost = 0;
            for (int i = 0; i < geneLength - 1; i++)
            {
                FitnessCost += TSP.costMatrix[genes[i]][genes[i+1]];
                FitnessDistance += TSP.distanceMatrix[genes[i]][genes[i + 1]];
            }
        }

        /// <summary>
        /// Returns true if this Individual dominates specified one, otherwise false
        /// </summary>
        /// <param name="a">Specified Individual</param>
        /// <returns></returns>
        public bool Dominates(Individual a)
        {
            if ((this.FitnessDistance < a.FitnessDistance && this.FitnessCost <= a.FitnessCost) ||
                (this.FitnessDistance <= a.FitnessDistance && this.FitnessCost < a.FitnessCost))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Converts genes to string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string toReturn = genes[0].ToString();
            for (int i = 1; i < genes.Count; i++)
            {
                toReturn += ", " + genes[i];
            }
            return toReturn;
        }

        /// <summary>
        /// Converts genes to string in reverse order
        /// </summary>
        /// <returns></returns>
        public string ReverseToString()
        {
            string toReturn = "" + genes[genes.Count - 1];
            for (int i = genes.Count - 2; i >= 0; i--)
            {
                toReturn += ", " + genes[i];
            }
            return toReturn;
        }
    }
}
