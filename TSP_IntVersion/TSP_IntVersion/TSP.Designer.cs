﻿namespace TSP_IntVersion
{
    partial class TSP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.startButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.generationsTextBox = new System.Windows.Forms.TextBox();
            this.populationTextBox = new System.Windows.Forms.TextBox();
            this.mutationTextBox = new System.Windows.Forms.TextBox();
            this.crossoverTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tournamentTextBox = new System.Windows.Forms.TextBox();
            this.currentRunChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.minMaxGridView = new System.Windows.Forms.DataGridView();
            this.Solution = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Distance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paretoFrontsChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btnClearChart = new System.Windows.Forms.Button();
            this.btnAddToChart = new System.Windows.Forms.Button();
            this.distanceFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.browseDistanceButton = new System.Windows.Forms.Button();
            this.browseCostButton = new System.Windows.Forms.Button();
            this.distancePathTextBox = new System.Windows.Forms.TextBox();
            this.costPathTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.costFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.label8 = new System.Windows.Forms.Label();
            this.gridViewSolutions = new System.Windows.Forms.DataGridView();
            this.RowNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Route = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalDistace = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Front = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.showHideSolutions = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.currentRunChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minMaxGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paretoFrontsChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSolutions)).BeginInit();
            this.SuspendLayout();
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(340, 243);
            this.startButton.Margin = new System.Windows.Forms.Padding(4);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(100, 28);
            this.startButton.TabIndex = 0;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(113, 87);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Number of Generations:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(113, 119);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Population size:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(113, 151);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Mutation rate:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(113, 183);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Cross-over rate:";
            // 
            // generationsTextBox
            // 
            this.generationsTextBox.Location = new System.Drawing.Point(308, 84);
            this.generationsTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.generationsTextBox.Name = "generationsTextBox";
            this.generationsTextBox.Size = new System.Drawing.Size(132, 22);
            this.generationsTextBox.TabIndex = 7;
            this.generationsTextBox.Text = "100";
            // 
            // populationTextBox
            // 
            this.populationTextBox.Location = new System.Drawing.Point(308, 116);
            this.populationTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.populationTextBox.Name = "populationTextBox";
            this.populationTextBox.Size = new System.Drawing.Size(132, 22);
            this.populationTextBox.TabIndex = 8;
            this.populationTextBox.Text = "100";
            // 
            // mutationTextBox
            // 
            this.mutationTextBox.Location = new System.Drawing.Point(308, 148);
            this.mutationTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.mutationTextBox.Name = "mutationTextBox";
            this.mutationTextBox.Size = new System.Drawing.Size(132, 22);
            this.mutationTextBox.TabIndex = 9;
            this.mutationTextBox.Text = "0.5";
            // 
            // crossoverTextBox
            // 
            this.crossoverTextBox.Location = new System.Drawing.Point(308, 180);
            this.crossoverTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.crossoverTextBox.Name = "crossoverTextBox";
            this.crossoverTextBox.Size = new System.Drawing.Size(132, 22);
            this.crossoverTextBox.TabIndex = 10;
            this.crossoverTextBox.Text = "0.9";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(113, 215);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 17);
            this.label6.TabIndex = 11;
            this.label6.Text = "Tournament Size:";
            // 
            // tournamentTextBox
            // 
            this.tournamentTextBox.Location = new System.Drawing.Point(308, 212);
            this.tournamentTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.tournamentTextBox.Name = "tournamentTextBox";
            this.tournamentTextBox.Size = new System.Drawing.Size(132, 22);
            this.tournamentTextBox.TabIndex = 12;
            this.tournamentTextBox.Text = "2";
            // 
            // currentRunChart
            // 
            chartArea1.Name = "ChartArea1";
            this.currentRunChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.currentRunChart.Legends.Add(legend1);
            this.currentRunChart.Location = new System.Drawing.Point(616, 283);
            this.currentRunChart.Margin = new System.Windows.Forms.Padding(4);
            this.currentRunChart.Name = "currentRunChart";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.currentRunChart.Series.Add(series1);
            this.currentRunChart.Size = new System.Drawing.Size(600, 431);
            this.currentRunChart.TabIndex = 13;
            this.currentRunChart.Text = "chart1";
            // 
            // minMaxGridView
            // 
            this.minMaxGridView.AllowUserToAddRows = false;
            this.minMaxGridView.AllowUserToDeleteRows = false;
            this.minMaxGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.minMaxGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Solution,
            this.Distance,
            this.Cost});
            this.minMaxGridView.Location = new System.Drawing.Point(717, 85);
            this.minMaxGridView.Margin = new System.Windows.Forms.Padding(4);
            this.minMaxGridView.Name = "minMaxGridView";
            this.minMaxGridView.ReadOnly = true;
            this.minMaxGridView.RowHeadersVisible = false;
            this.minMaxGridView.Size = new System.Drawing.Size(404, 185);
            this.minMaxGridView.TabIndex = 14;
            // 
            // Solution
            // 
            this.Solution.HeaderText = "Solution";
            this.Solution.Name = "Solution";
            this.Solution.ReadOnly = true;
            // 
            // Distance
            // 
            this.Distance.HeaderText = "Distance";
            this.Distance.Name = "Distance";
            this.Distance.ReadOnly = true;
            // 
            // Cost
            // 
            this.Cost.HeaderText = "Cost";
            this.Cost.Name = "Cost";
            this.Cost.ReadOnly = true;
            // 
            // paretoFrontsChart
            // 
            chartArea2.Name = "ChartArea1";
            this.paretoFrontsChart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.paretoFrontsChart.Legends.Add(legend2);
            this.paretoFrontsChart.Location = new System.Drawing.Point(8, 283);
            this.paretoFrontsChart.Margin = new System.Windows.Forms.Padding(4);
            this.paretoFrontsChart.Name = "paretoFrontsChart";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.paretoFrontsChart.Series.Add(series2);
            this.paretoFrontsChart.Size = new System.Drawing.Size(600, 431);
            this.paretoFrontsChart.TabIndex = 13;
            this.paretoFrontsChart.Text = "chart1";
            // 
            // btnClearChart
            // 
            this.btnClearChart.Location = new System.Drawing.Point(116, 243);
            this.btnClearChart.Margin = new System.Windows.Forms.Padding(4);
            this.btnClearChart.Name = "btnClearChart";
            this.btnClearChart.Size = new System.Drawing.Size(100, 28);
            this.btnClearChart.TabIndex = 0;
            this.btnClearChart.Text = "Clear chart";
            this.btnClearChart.UseVisualStyleBackColor = true;
            this.btnClearChart.Click += new System.EventHandler(this.btnClearChart_Click);
            // 
            // btnAddToChart
            // 
            this.btnAddToChart.Location = new System.Drawing.Point(228, 243);
            this.btnAddToChart.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddToChart.Name = "btnAddToChart";
            this.btnAddToChart.Size = new System.Drawing.Size(100, 28);
            this.btnAddToChart.TabIndex = 0;
            this.btnAddToChart.Text = "Add to chart";
            this.btnAddToChart.UseVisualStyleBackColor = true;
            this.btnAddToChart.Click += new System.EventHandler(this.btnAddToChart_Click);
            // 
            // distanceFileDialog
            // 
            this.distanceFileDialog.FileName = "openFileDialog1";
            // 
            // browseDistanceButton
            // 
            this.browseDistanceButton.Location = new System.Drawing.Point(446, 9);
            this.browseDistanceButton.Name = "browseDistanceButton";
            this.browseDistanceButton.Size = new System.Drawing.Size(100, 28);
            this.browseDistanceButton.TabIndex = 15;
            this.browseDistanceButton.Text = "Browse";
            this.browseDistanceButton.UseVisualStyleBackColor = true;
            this.browseDistanceButton.Click += new System.EventHandler(this.browseDistanceButton_Click);
            // 
            // browseCostButton
            // 
            this.browseCostButton.Location = new System.Drawing.Point(446, 37);
            this.browseCostButton.Name = "browseCostButton";
            this.browseCostButton.Size = new System.Drawing.Size(100, 28);
            this.browseCostButton.TabIndex = 16;
            this.browseCostButton.Text = "Browse";
            this.browseCostButton.UseVisualStyleBackColor = true;
            this.browseCostButton.Click += new System.EventHandler(this.browseCostButton_Click);
            // 
            // distancePathTextBox
            // 
            this.distancePathTextBox.Location = new System.Drawing.Point(187, 12);
            this.distancePathTextBox.Name = "distancePathTextBox";
            this.distancePathTextBox.Size = new System.Drawing.Size(253, 22);
            this.distancePathTextBox.TabIndex = 17;
            // 
            // costPathTextBox
            // 
            this.costPathTextBox.Location = new System.Drawing.Point(187, 40);
            this.costPathTextBox.Name = "costPathTextBox";
            this.costPathTextBox.Size = new System.Drawing.Size(253, 22);
            this.costPathTextBox.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(113, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 17);
            this.label1.TabIndex = 19;
            this.label1.Text = "Distance:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(113, 43);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 17);
            this.label7.TabIndex = 20;
            this.label7.Text = "Cost:";
            // 
            // costFileDialog
            // 
            this.costFileDialog.FileName = "openFileDialog1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 736);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(202, 17);
            this.label8.TabIndex = 21;
            this.label8.Text = "Unique Optimal Routes Found:";
            // 
            // gridViewSolutions
            // 
            this.gridViewSolutions.AllowUserToAddRows = false;
            this.gridViewSolutions.AllowUserToDeleteRows = false;
            this.gridViewSolutions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridViewSolutions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RowNumber,
            this.Route,
            this.TotalDistace,
            this.TotalCost,
            this.Front});
            this.gridViewSolutions.Location = new System.Drawing.Point(8, 71);
            this.gridViewSolutions.Name = "gridViewSolutions";
            this.gridViewSolutions.ReadOnly = true;
            this.gridViewSolutions.RowTemplate.Height = 24;
            this.gridViewSolutions.Size = new System.Drawing.Size(1270, 598);
            this.gridViewSolutions.TabIndex = 22;
            this.gridViewSolutions.Visible = false;
            // 
            // RowNumber
            // 
            this.RowNumber.HeaderText = "No.";
            this.RowNumber.Name = "RowNumber";
            this.RowNumber.ReadOnly = true;
            this.RowNumber.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.RowNumber.Width = 50;
            // 
            // Route
            // 
            this.Route.HeaderText = "Route";
            this.Route.Name = "Route";
            this.Route.ReadOnly = true;
            this.Route.Width = 500;
            // 
            // TotalDistace
            // 
            this.TotalDistace.HeaderText = "Total Distance";
            this.TotalDistace.Name = "TotalDistace";
            this.TotalDistace.ReadOnly = true;
            this.TotalDistace.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // TotalCost
            // 
            this.TotalCost.HeaderText = "Total Cost";
            this.TotalCost.Name = "TotalCost";
            this.TotalCost.ReadOnly = true;
            this.TotalCost.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Front
            // 
            this.Front.HeaderText = "Front";
            this.Front.Name = "Front";
            this.Front.ReadOnly = true;
            this.Front.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Front.Width = 50;
            // 
            // showHideSolutions
            // 
            this.showHideSolutions.Location = new System.Drawing.Point(1165, 8);
            this.showHideSolutions.Name = "showHideSolutions";
            this.showHideSolutions.Size = new System.Drawing.Size(100, 29);
            this.showHideSolutions.TabIndex = 23;
            this.showHideSolutions.Text = "Show/Hide Solutions";
            this.showHideSolutions.UseVisualStyleBackColor = true;
            this.showHideSolutions.Click += new System.EventHandler(this.showHideSolutions_Click);
            // 
            // TSP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1290, 674);
            this.Controls.Add(this.showHideSolutions);
            this.Controls.Add(this.gridViewSolutions);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.costPathTextBox);
            this.Controls.Add(this.distancePathTextBox);
            this.Controls.Add(this.browseCostButton);
            this.Controls.Add(this.browseDistanceButton);
            this.Controls.Add(this.minMaxGridView);
            this.Controls.Add(this.paretoFrontsChart);
            this.Controls.Add(this.currentRunChart);
            this.Controls.Add(this.tournamentTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.crossoverTextBox);
            this.Controls.Add(this.mutationTextBox);
            this.Controls.Add(this.populationTextBox);
            this.Controls.Add(this.generationsTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnAddToChart);
            this.Controls.Add(this.btnClearChart);
            this.Controls.Add(this.startButton);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "TSP";
            this.Text = "TSP";
            ((System.ComponentModel.ISupportInitialize)(this.currentRunChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minMaxGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paretoFrontsChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSolutions)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox generationsTextBox;
        private System.Windows.Forms.TextBox populationTextBox;
        private System.Windows.Forms.TextBox mutationTextBox;
        private System.Windows.Forms.TextBox crossoverTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tournamentTextBox;
        private System.Windows.Forms.DataVisualization.Charting.Chart currentRunChart;
        private System.Windows.Forms.DataGridView minMaxGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Solution;
        private System.Windows.Forms.DataGridViewTextBoxColumn Distance;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cost;
        private System.Windows.Forms.DataVisualization.Charting.Chart paretoFrontsChart;
        private System.Windows.Forms.Button btnClearChart;
        private System.Windows.Forms.Button btnAddToChart;
        private System.Windows.Forms.OpenFileDialog distanceFileDialog;
        private System.Windows.Forms.Button browseDistanceButton;
        private System.Windows.Forms.Button browseCostButton;
        private System.Windows.Forms.TextBox distancePathTextBox;
        private System.Windows.Forms.TextBox costPathTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.OpenFileDialog costFileDialog;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView gridViewSolutions;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Route;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalDistace;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn Front;
        private System.Windows.Forms.Button showHideSolutions;
    }
}

